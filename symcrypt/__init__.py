import hashlib
import importlib
import inspect

from flask import Flask, session, g, request, jsonify, abort, url_for
from flask_login import LoginManager, login_user
from flask_restful import Api

from mongoengine import connect
from symcrypt.user.models import UserModel
from symcrypt.conf import MONGO_HOST, MONGO_PORT, SECRET_KEY, INSTALLED_MODULES

connect('symcrypt', host=MONGO_HOST, port=MONGO_PORT)

app = Flask(__name__)
app.secret_key = SECRET_KEY

login_manager = LoginManager()
login_manager.init_app(app)

api = Api(app)


@login_manager.user_loader
def load_user(user):
    return UserModel.objects(username=user).first()


@app.route('/user/login', methods=['POST'])
def login():
    u = UserModel.objects(username=request.form['username']).first()
    if u:
        if u.password == request.form['password']:
            login_user(u)
            return jsonify(
                {
                    'err': '',
                }
            ), 200
        else:
            return jsonify(
                {
                    'err': 'username or password is not correct',
                }
            ), 400
    else:
        return jsonify(
            {
                'err': 'the user does not exist'
            }
        ), 404


@app.route('/user/forgot-password', methods=['POST'])
def forgot_password():
    """
        The hash function is not suitable in here
        It should be better to use flask-security package
        However I got into problem setting up mongodb,
        So I cant start test.py, the token should have expire time
    """
    u = UserModel.objects(username=request.form['username']).first()
    if u:
        # generate reset link for user
        h = hashlib.new('ripemd160')
        h.update(u.username)
        # its look like django !
        u.reset_password_token = h.hexdigest()
        u.save()
        # email the following link to user,  its better the send in the background thread
        # also it should be notify using try catch that it is sent successfully or not
        send_mail(request.base_url+'/user/reset-password/'+u.token)
        return jsonify(
            {
                'err': '',
            }
        ), 200
    else:
        return jsonify(
            {
                'err': 'the user does not exist'
            }
        ), 404


@app.route('/user/reset-password/<string:token>', methods=['POST'])
def reset_password(token):
    """
       assumes that client send the new password and its confirm
       if we use templates this endpoint should render the view and
       another endpoint should be added for changing the password
    """
    u = UserModel.objects(token=token).first()
    if u and validate_token(token):
        new_password = request.form['new_password']
        new_password_confirm = request.form['new_password_confirm']
        if new_password == new_password_confirm:
            u.set_password(new_password)
            u.save()
            login_user(u)
        else:
            return jsonify(
                {
                    'err': 'Password is not match with confirm'
                }
            ), 400
    else:
        return jsonify(
            {
                'err': 'Token is not valid'
            }
        ), 404


# these utility functions should be moved to another package or file
def send_mail(url):
    pass


def validate_token(token):
    pass


for pkg in INSTALLED_MODULES:
    service_script = importlib.import_module(
        "symcrypt.%s.resources" % pkg)
    admin_script = importlib.import_module('symcrypt.%s.models' % pkg)
    for name, obj in inspect.getmembers(service_script):
        if inspect.isclass(obj) and 'symcrypt.%s' % pkg in str(obj) and 'Resource' in str(obj):
            api.add_resource(
                obj,
                '/%s/%s' % (pkg, obj.url),
                endpoint='%s.%s' % (pkg, obj.__name__)
            )
    for name, obj in inspect.getmembers(admin_script):
        if inspect.isclass(obj) and 'api.%s' % pkg in str(obj) and 'Model' in str(obj):
            admin.add_view(ModelView(obj))
